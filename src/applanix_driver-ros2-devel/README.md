# Applanix ROS driver
This package allows parsing of a subset of the groups described in the Applanix's ICD (ref. PUBS-ICD-003759, 
ENG-ICD-000027, PUBS-ICD-006232) for publication into ROS as well as a subset of Trimble's [GSOF records](https://www.trimble.com/OEM_ReceiverHelp/v5.11/en/Default.html#GSOFmessages_Overview.html%3FTocPath%3DOutput%2520Messages%7CGSOF%2520Messages%7C_____1).

## ROS version compatibility

ROS1 and ROS2 support has been split between two branches: `ros1-devel` and `ros2-devel`. ROS2 is actively built for `dashing`.