#!/bin/bash

set -e

source /home/projects/ozkan_ws/install/setup.bash
cd /home/projects/ozkan_ws/
colcon build --cmake-args -DBUILD_TESTS=ON
