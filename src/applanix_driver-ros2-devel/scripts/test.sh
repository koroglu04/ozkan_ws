#!/bin/bash

set -e

bin_path='/home/colcon_ws/build/applanix_driver/test/applanix_driver_test'
test_data_path='/home/colcon_ws/src/applanix_driver/applanix_driver/test/data'

source /home/colcon_ws/install/setup.bash

cd ${test_data_path}
${bin_path} --gtest_output=xml:/home/
