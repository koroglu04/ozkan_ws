#!/bin/bash

set -x
set -v
set -e

ROS_DISTRO=$(rosversion -d)
UBUNTU_DISTRO=$(lsb_release -c -s)

apt-get update && apt-get install -y \
	apt-transport-https \
	ca-certificates \
	gnupg \
	software-properties-common \
  build-essential \
  curl \
  libgeographic-dev \
  libpcap-dev \
  p11-kit \
  python3-catkin-tools \
  ros-foxy-geometry-msgs \
  ros-foxy-nav-msgs \
  ros-foxy-sensor-msgs \
  ros-foxy-tf2 \
  ros-foxy-tf2-geometry-msgs \
  tmux \
  wget

wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | apt-key add -

apt-add-repository "deb https://apt.kitware.com/ubuntu/ ${UBUNTU_DISTRO} main"
add-apt-repository ppa:ubuntu-toolchain-r/test -y
apt-get update

apt-get install kitware-archive-keyring
apt-key --keyring /etc/apt/trusted.gpg del C1F34CDD40CD72DA

apt-get install -y cmake g++-7

update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 10
update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-7 10

# Install git lfs to be able to pull test data
(curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash) && \
  apt-get install -y git-lfs && \
  git lfs install

# Install Trimble CA certificate for SSL connection to Gitlab
# convert certificate from DER to PEM format for installation.
wget http://certsrv.trimble.com/ca3cert.crt -O /ca3cert.crt && \
  openssl x509 -in ca3cert.crt -inform der -outform pem -out ca3cert.pem && \
  mv ca3cert.pem /usr/local/share/ca-certificates/ca3cert.crt && \
  update-ca-certificates
  
