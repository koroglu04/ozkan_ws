#include "applanix_driver/lv_client.h"
#include "applanix_driver/icd/icd_vector.h"
#include "applanix_driver/icd/group.h"
#include "applanix_driver/icd/message.h"
#include "applanix_driver/icd/message_parser.h"

#include "network/pcap_tcp_client.h"
#include "network/tcp_client_delimiter.h"
#include "network/udp_client.h"

namespace applanix_driver {

LvClient::LvClient(const std::string &lv_ip, unsigned int lv_port, bool enable_display_port,
                   std::optional<std::string> pcap_filename) :
    keep_running_(true) {
  if (pcap_filename) {
    tcp_client_ = std::make_unique<network::PcapTcpClient>(lv_ip, lv_port, *pcap_filename,
                                                           icd::group::START, icd::group::END);
  } else {
    tcp_client_ = std::make_unique<network::TcpClientDelimiter>(lv_ip, lv_port, icd::group::START, icd::group::END);
  }
  if (enable_display_port) {
    udp_client_ = std::make_unique<network::UdpClient>(lv_ip, 5600);
  }
}

LvClient::~LvClient() {
  stop();
  if (tcp_thread_->joinable()) {
    tcp_thread_->join();
  }
  if (udp_thread_->joinable()) {
    udp_thread_->join();
  }
}

util::Status LvClient::start() {
  util::Status status = tcp_client_->open();
  if (!status) return status;
  tcp_thread_ = std::make_unique<std::thread>(&LvClient::runTcpConnection, this);

  if (udp_client_) {
    status = udp_client_->open();
    if (!status) return status;
    udp_thread_ = std::make_unique<std::thread>(&LvClient::runUdpConnection, this);
  }

  return status;
}

void LvClient::stop() {
  keep_running_ = false;
}

std::list<LvClient::GroupCallback>::iterator LvClient::registerCallback(icd::group::Id group,
                                                                        const LvClient::GroupCallback &callback) {
  if (group_callbacks_.count(group) == 0) {
    group_callbacks_[group] = std::list<GroupCallback>();
  }

  group_callbacks_[group].push_back(callback);

  return --group_callbacks_[group].end();
}

std::list<LvClient::MessageCallback>::iterator LvClient::registerCallback(icd::message::Id msg,
                                                                          const LvClient::MessageCallback &callback) {
  if (msg_callbacks_.count(msg) == 0) {
    msg_callbacks_[msg] = std::list<MessageCallback>();
  }

  msg_callbacks_[msg].push_back(callback);
  return --msg_callbacks_[msg].end();
}

void LvClient::unregisterCallback(applanix_driver::icd::group::Id group,
                                  const std::list<GroupCallback>::iterator &it) {
  if (group_callbacks_.count(group) == 0) return;
  group_callbacks_[group].erase(it);
}

void LvClient::unregisterCallback(applanix_driver::icd::message::Id msg,
                                  const std::list<MessageCallback>::iterator &it) {
  if (msg_callbacks_.count(msg) == 0) return;
  msg_callbacks_[msg].erase(it);
}

void LvClient::runTcpConnection() {
  while (keep_running_.load()) {
    grabAndParseTcp();
  }
}

void LvClient::runUdpConnection() {
  while (keep_running_.load()) {
    grabAndParseUdp();
  }
}

void LvClient::grabAndParseTcp() {
  int bytes_rcvd = tcp_client_->receive();

  if (bytes_rcvd < 0) {
    // Connection closed
    keep_running_.store(false);
    return;
  }

  icd::GroupVector group_vector(reinterpret_cast<const std::byte *>(tcp_client_->getBuffer().data()), bytes_rcvd);
  if (!group_vector.isValid())
    return;

  for (const auto &group : group_vector) {
    auto group_callbacks = group_callbacks_.find(group.getId());
    if (group_callbacks == group_callbacks_.end())
      continue;

    for (const auto &callback : group_callbacks->second) {
      callback(group);
    }
  }
}

void LvClient::grabAndParseUdp() {
  int bytes_rcvd = udp_client_->receive();
  if (bytes_rcvd < 1) return;

  icd::MessageVector message_vector(reinterpret_cast<const std::byte *>(udp_client_->getBuffer().data()), bytes_rcvd);
  if (!message_vector.isValid())
    return;

  for (const auto &message : message_vector) {
    auto message_callbacks = msg_callbacks_.find(message.getId());
    if (message_callbacks == msg_callbacks_.end())
      continue;

    for (const auto &callback : message_callbacks->second) {
      callback(message);
    }
  }
}

}  // namespace applanix_driver
