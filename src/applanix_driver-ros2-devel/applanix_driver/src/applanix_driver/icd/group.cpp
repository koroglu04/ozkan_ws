#include "applanix_driver/icd/group.h"

namespace applanix_driver::icd {

void group::Group3::fillHeader(const ConstantHeader &constant_header) {
  header = constant_header.header;
  time_distance = constant_header.time_distance;
  nav_solution_status = constant_header.nav_solution_status;
  num_of_sv_tracked = constant_header.num_of_sv_tracked;
  channel_status_byte_count = constant_header.channel_status_byte_count;
}

void group::Group3::fillFooter(const ConstantFooter &constant_footer) {
  HDOP = constant_footer.HDOP;
  VDOP = constant_footer.VDOP;
  dgps_correction_latency = constant_footer.dgps_correction_latency;
  dgps_reference_id = constant_footer.dgps_reference_id;
  gps_week = constant_footer.gps_week;
  utc_time_offset = constant_footer.utc_time_offset;
  gnss_nav_msg_latency = constant_footer.gnss_nav_msg_latency;
  geoidal_separation = constant_footer.geoidal_separation;
  gns_srcv_type = constant_footer.gns_srcv_type;
  gnss_status = constant_footer.gnss_status;
  memcpy(padding, constant_footer.pad, sizeof(padding));
  checksum = constant_footer.checksum;
}

void group::Group100002::fillHeader(const ConstantHeader &constant_header) {
  header = constant_header.header;
  time_distance = constant_header.time_distance;
  imu_header = constant_header.imu_header;
  imu_byte_count = constant_header.imu_byte_count;
}

void group::Group100002::fillFooter(const ConstantFooter &constant_footer) {
  data_checksum = constant_footer.data_checksum;
  checksum = constant_footer.checksum;
}

}  // namespace applanix_driver::icd
