#include "applanix_driver/icd/message_parser.h"

#include <algorithm>
#include <cstring>

namespace applanix_driver::icd::message {

const std::set<Id> MessageParser::supported_messages_ = {
    ID_GENERAL_PARAMS
};

MessageParser::MessageParser(const std::byte *data, std::size_t length) {
  // Assert some things about the platform to make sure memcpy operates correctly
  static_assert(sizeof(float) == 4);
  static_assert(sizeof(double) == 8);

  this->setData(data, length);
}

void MessageParser::setData(const std::byte *data, std::size_t length) {
  data_ = data;
  length_ = length;
}

bool MessageParser::isValid() const {
  static constexpr size_t MIN_SIZE = START.size() + sizeof(Header) + END.size() + sizeof(Footer);
  if (length_ <= MIN_SIZE) return false;

  if (std::memcmp(data_, START.data(), START.size()) != 0) return false;

  // TODO(Andre) compute checksum

  return std::memcmp(data_ + length_ - END.size(), END.data(), END.size()) == 0;
}

bool MessageParser::isSupported() const {
  Header header;
  std::memcpy(&header, data_, sizeof(header));
  return std::find(supported_messages_.begin(), supported_messages_.end(), header.id) != supported_messages_.end();
}

Id MessageParser::getId() const {
  return getHeader().id;
}

Header MessageParser::getHeader() const {
  Header header;
  std::memcpy(&header, data_ + Header::OFFSET, sizeof(header));
  return header;
}

GeneralParams MessageParser::getGeneralParams() const {
  return typeCastMessage<GeneralParams>();
}

std::size_t MessageParser::getStartBytesSize() const {
  return START.size() + sizeof(Header);
}

}  // namespace applanix_driver::icd::message
