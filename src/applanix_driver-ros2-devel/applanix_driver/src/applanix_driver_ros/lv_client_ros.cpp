#include "applanix_driver_ros/lv_client_ros.h"

#include <string>
#include <sstream>

#include <applanix_msgs/msg/navigation_solution_group1.hpp>
#include <GeographicLib/MGRS.hpp>
#include <GeographicLib/UTMUPS.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <sensor_msgs/msg/nav_sat_fix.hpp>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include "applanix_driver_ros/conversions.h"

namespace applanix_driver_ros {

LvClientRos::LvClientRos(const rclcpp::NodeOptions &options) :
    Node(k_default_node_name, options),
    parent_frame_id_(),
    child_frame_id_(),
    time_source_(util::RosTimeSource::GPS_TIME_OF_WEEK),
    ros_clock_(RCL_ROS_TIME),
    publish_icd_msgs_(false),
    publish_ros_msgs_(false),
    transform_broadcaster_(std::nullopt),
    static_tf_broadcaster_(std::nullopt),
    vehicle_nav_solution_(std::nullopt),
    vehicle_nav_rms_(std::nullopt),
    primary_gnss_status_(std::nullopt) {
  auto ip = this->declare_parameter<std::string>("lv_ip", "172.16.20.12");
  auto port = this->declare_parameter<int>("port", 5602);
  auto enable_display_port = this->declare_parameter<bool>("enable_display_port", false);

  rclcpp::Parameter parameter;
  bool pcap_found = this->get_parameter("pcap_file", parameter);

  parent_frame_id_ = this->declare_parameter<std::string>("parent_frame", "ned");
  child_frame_id_ = this->declare_parameter<std::string>("child_frame", "POS_REF");
  parent_frame_utm_id_ = this->declare_parameter<std::string>("parent_frame_utm", "utm");
  child_frame_utm_id_ = this->declare_parameter<std::string>("child_frame_utm", "POS_REF_UTM");
  frame_id_vehicle_ = child_frame_id_ + "_vehicle";

  publish_tf_ = this->declare_parameter<bool>("publish_tf", true);
  publish_tf_utm_ = this->declare_parameter<bool>("publish_tf_utm", true);
  publish_icd_msgs_ = this->declare_parameter<bool>("publish_icd_msgs", true);
  publish_ros_msgs_ = this->declare_parameter<bool>("publish_ros_msgs", true);
  std::string time_source = this->declare_parameter<std::string>("time_source", "gps_time_of_week");
  time_source_ = util::toRosTimeSource(time_source);

  if (pcap_found && !parameter.as_string().empty()) {
    lv_client_.emplace(ip, port, enable_display_port, parameter.as_string());
  } else {
    lv_client_.emplace(ip, port, enable_display_port, std::nullopt);
  }

  namespace group = applanix_driver::icd::group;
  namespace message = applanix_driver::icd::message;

  registerCallback(group::GROUP_ID_1, &LvClientRos::saveGroup1Callback);
  registerCallback(group::GROUP_ID_2, &LvClientRos::saveGroup2Callback);
  registerCallback(group::GROUP_ID_3, &LvClientRos::saveGroup3Callback);
  registerCallback(message::ID_GENERAL_PARAMS, &LvClientRos::publishMessage20Callback);

  if (publish_ros_msgs_) {
    registerAndAdvertise<nav_msgs::msg::Odometry>(group::GROUP_ID_1,
                                                  &LvClientRos::publishOdometryAndNavSatCallback,
                                                  k_topic_odometry);

    // These are custom messages but there is no appropriate message in common_msgs
    registerAndAdvertise<applanix_msgs::msg::EventGroup>(group::GROUP_ID_5,
                                                         &LvClientRos::publishGroup5Callback,
                                                         k_topic_event_1);
    registerAndAdvertise<applanix_msgs::msg::EventGroup>(group::GROUP_ID_6,
                                                         &LvClientRos::publishGroup6Callback,
                                                         k_topic_event_2);
    registerAndAdvertise<applanix_msgs::msg::EventGroup>(group::GROUP_ID_30,
                                                         &LvClientRos::publishGroup30Callback,
                                                         k_topic_event_3);
    registerAndAdvertise<applanix_msgs::msg::EventGroup>(group::GROUP_ID_31,
                                                         &LvClientRos::publishGroup31Callback,
                                                         k_topic_event_4);
    registerAndAdvertise<applanix_msgs::msg::EventGroup>(group::GROUP_ID_32,
                                                         &LvClientRos::publishGroup32Callback,
                                                         k_topic_event_5);
    registerAndAdvertise<applanix_msgs::msg::EventGroup>(group::GROUP_ID_33,
                                                         &LvClientRos::publishGroup33Callback,
                                                         k_topic_event_6);

    publishers_[k_topic_navsat] =
        this->create_publisher<sensor_msgs::msg::NavSatFix>(k_topic_navsat, k_default_qos_history_depth);
    publishers_[k_topic_utm] =
        this->create_publisher<nav_msgs::msg::Odometry>(k_topic_utm, k_default_qos_history_depth);
  }

  if (publish_icd_msgs_) {
    registerAndAdvertise<applanix_msgs::msg::NavigationSolutionGroup1>(group::GROUP_ID_1,
                                                                       &LvClientRos::publishGroup1Callback,
                                                                       k_topic_group_1);
    registerAndAdvertise<applanix_msgs::msg::NavigationPerformanceGroup2>(group::GROUP_ID_2,
                                                                          &LvClientRos::publishGroup2Callback,
                                                                          k_topic_group_2);
    registerAndAdvertise<applanix_msgs::msg::TimeTaggedImuDataGroup4>(group::GROUP_ID_4,
                                                                      &LvClientRos::publishGroup4Callback,
                                                                      k_topic_group_4);
  }

  createService<applanix_msgs::srv::SetOrigin>(k_service_set_origin, &LvClientRos::setOriginCallback);
  createService<applanix_msgs::srv::GetUtmZone>(k_service_get_utm_zone, &LvClientRos::getUtmZoneCallback);
  createService<applanix_msgs::srv::GetMgrsZone>(k_service_get_mgrs_zone, &LvClientRos::getMgrsZoneCallback);

  if (publish_tf_ || publish_tf_utm_) {
    transform_broadcaster_.emplace(this);
    static_tf_broadcaster_.emplace(this);
  }

  util::Status status = lv_client_->start();
  if (!status.ok()) {
    RCLCPP_ERROR(this->get_logger(),
                 "POS LV Client failed to connect to %s:%d with error %s",
                 ip.c_str(),
                 port,
                 status.toString().c_str());
    throw std::runtime_error(status.toString());
  }

  RCLCPP_INFO(this->get_logger(), "Connected to POS LV %s:%d", ip.c_str(), port);
}

template<typename RosMessageType>
void LvClientRos::registerAndAdvertise(applanix_driver::icd::group::Id group_id,
                                        LvClientRos::GroupCallback callback,
                                        const std::string &topic) {
  registerCallback(group_id, callback);
  publishers_[topic] = this->create_publisher<RosMessageType>(topic, k_default_qos_history_depth);
}

void LvClientRos::saveGroup1Callback(const GroupParser &group_parser) {
  vehicle_nav_solution_.emplace(group_parser.as<applanix_driver::icd::group::NavSolution>());
}

void LvClientRos::saveGroup2Callback(const GroupParser &group_parser) {
  vehicle_nav_rms_.emplace(group_parser.as<applanix_driver::icd::group::VehicleNavRms>());
}

void LvClientRos::saveGroup3Callback(const GroupParser &group_parser) {
  primary_gnss_status_ = group_parser.as<applanix_driver::icd::group::PrimaryGnssStatus>();
}

void LvClientRos::registerCallback(applanix_driver::icd::group::Id group_id, LvClientRos::GroupCallback callback) {
  lv_client_->registerCallback(group_id, std::bind(callback, this, std::placeholders::_1));
}

void LvClientRos::registerCallback(applanix_driver::icd::message::Id message_id, LvClientRos::MessageCallback callback) {
  lv_client_->registerCallback(message_id, std::bind(callback, this, std::placeholders::_1));
}

void LvClientRos::publishOdometryAndNavSatCallback(const applanix_driver::icd::group::GroupParser &) {
  const auto &lla = vehicle_nav_solution_->lla;

  {
    std::lock_guard<std::mutex> guard(mtx_local_origin_);
    if (!local_cartesian_) {
      RCLCPP_INFO(rclcpp::get_logger(this->get_name()),
                  "NED origin initializing to Lat: %4.5f Lon: %4.5f Alt: %4.5f",
                  lla.latitude,
                  lla.longitude,
                  lla.altitude);
      local_cartesian_.emplace(lla.latitude, lla.longitude, lla.altitude);
    }
  }

  nav_msgs::msg::Odometry odometry = vehicle_nav_rms_
                                     ? toOdometry(*vehicle_nav_solution_, *local_cartesian_, *vehicle_nav_rms_)
                                     : toOdometry(*vehicle_nav_solution_, *local_cartesian_);

  if (time_source_ == util::RosTimeSource::NOW) {
    odometry.header.stamp = ros_clock_.now();
  }

  odometry.header.frame_id = parent_frame_id_;
  odometry.child_frame_id = child_frame_id_;

  using OdometryPublisher = rclcpp::Publisher<nav_msgs::msg::Odometry>;
  auto odom_pub = std::dynamic_pointer_cast<OdometryPublisher>(publishers_[k_topic_odometry]);
  odom_pub->publish(odometry);

  nav_msgs::msg::Odometry odometry_utm = vehicle_nav_rms_
                                         ? toUtm(*vehicle_nav_solution_, *vehicle_nav_rms_)
                                         : toUtm(*vehicle_nav_solution_);
  odometry_utm.header.stamp = odometry.header.stamp;
  odometry_utm.header.frame_id = parent_frame_utm_id_;
  odometry_utm.child_frame_id = child_frame_utm_id_;

  auto odom_pub_utm = std::dynamic_pointer_cast<OdometryPublisher>(publishers_[k_topic_utm]);
  odom_pub_utm->publish(odometry_utm);

  if (primary_gnss_status_) {
    sensor_msgs::msg::NavSatFix nav_sat_fix = vehicle_nav_rms_
                                              ? toNavSatFix(*vehicle_nav_solution_,
                                                            *primary_gnss_status_,
                                                            *vehicle_nav_rms_)
                                              : toNavSatFix(*vehicle_nav_solution_, *primary_gnss_status_);
    using NavSatPublisher = rclcpp::Publisher<sensor_msgs::msg::NavSatFix>;
    auto nav_sat_pub = std::dynamic_pointer_cast<NavSatPublisher>(publishers_[k_topic_navsat]);
    nav_sat_pub->publish(nav_sat_fix);
  }

  if (publish_tf_) {
    transform_broadcaster_->sendTransform(toTransformStamped(odometry.header,
                                                             odometry.child_frame_id,
                                                             odometry.pose.pose));
  }

  if (publish_tf_utm_) {
    transform_broadcaster_->sendTransform(toTransformStamped(odometry_utm.header,
                                                             odometry_utm.child_frame_id,
                                                             odometry_utm.pose.pose));
  }
}

void LvClientRos::publishGroup1Callback(const GroupParser &) {
  // Since saveGroup1Callback has already been called we can use our private vehicle nav solution
  auto msg = toRosMessage(*vehicle_nav_solution_);
  msg.header.frame_id = child_frame_id_;
  if (time_source_ == util::RosTimeSource::NOW) {
    msg.header.stamp = ros_clock_.now();
  }
  using Group1Publisher = rclcpp::Publisher<applanix_msgs::msg::NavigationSolutionGroup1>;
  auto group1_pub = std::dynamic_pointer_cast<Group1Publisher>(publishers_[k_topic_group_1]);
  group1_pub->publish(msg);
}

void LvClientRos::publishGroup2Callback(const applanix_driver::icd::group::GroupParser &) {
  auto msg = toRosMessage(*vehicle_nav_rms_);
  msg.header.frame_id = child_frame_id_;
  if (time_source_ == util::RosTimeSource::NOW) {
    msg.header.stamp = ros_clock_.now();
  }
  using Group2Publisher = rclcpp::Publisher<applanix_msgs::msg::NavigationPerformanceGroup2>;
  auto group2_pub = std::dynamic_pointer_cast<Group2Publisher>(publishers_[k_topic_group_2]);
  group2_pub->publish(msg);
}

void LvClientRos::publishGroup4Callback(const applanix_driver::icd::group::GroupParser &group_parser) {
  using applanix_driver::icd::group::Group4;
  auto msg = toRosMessage(group_parser.as<Group4>());
  msg.header.frame_id = child_frame_id_;
  if (time_source_ == util::RosTimeSource::NOW) {
    msg.header.stamp = ros_clock_.now();
  }
  using Group4Publisher = rclcpp::Publisher<applanix_msgs::msg::TimeTaggedImuDataGroup4>;
  auto group4_pub = std::dynamic_pointer_cast<Group4Publisher>(publishers_[k_topic_group_4]);
  group4_pub->publish(msg);
}

void LvClientRos::publishGroup5Callback(const applanix_driver::icd::group::GroupParser &group_parser) {
  using applanix_driver::icd::group::Group5;
  auto event = toRosMessage(group_parser.as<Group5>());
  event.header.frame_id = child_frame_id_;
  if (time_source_ == util::RosTimeSource::NOW) {
    event.header.stamp = ros_clock_.now();
  }
  using Group5Publisher = rclcpp::Publisher<applanix_msgs::msg::EventGroup>;
  auto group5_pub = std::dynamic_pointer_cast<Group5Publisher>(publishers_[k_topic_event_1]);
  group5_pub->publish(event);
}

void LvClientRos::publishGroup6Callback(const applanix_driver::icd::group::GroupParser &group_parser) {
  using applanix_driver::icd::group::Group6;
  auto event = toRosMessage(group_parser.as<Group6>());
  event.header.frame_id = child_frame_id_;
  if (time_source_ == util::RosTimeSource::NOW) {
    event.header.stamp = ros_clock_.now();
  }
  using Group6Publisher = rclcpp::Publisher<applanix_msgs::msg::EventGroup>;
  auto group6_pub = std::dynamic_pointer_cast<Group6Publisher>(publishers_[k_topic_event_2]);
  group6_pub->publish(event);
}

void LvClientRos::publishGroup30Callback(const applanix_driver::icd::group::GroupParser &group_parser) {
  using applanix_driver::icd::group::Group30;
  auto event = toRosMessage(group_parser.as<Group30>());
  event.header.frame_id = child_frame_id_;
  if (time_source_ == util::RosTimeSource::NOW) {
    event.header.stamp = ros_clock_.now();
  }
  using Group30Publisher = rclcpp::Publisher<applanix_msgs::msg::EventGroup>;
  auto group30_pub = std::dynamic_pointer_cast<Group30Publisher>(publishers_[k_topic_event_3]);
  group30_pub->publish(event);
}

void LvClientRos::publishGroup31Callback(const applanix_driver::icd::group::GroupParser &group_parser) {
  using applanix_driver::icd::group::Group31;
  auto event = toRosMessage(group_parser.as<Group31>());
  event.header.frame_id = child_frame_id_;
  if (time_source_ == util::RosTimeSource::NOW) {
    event.header.stamp = ros_clock_.now();
  }
  using Group31Publisher = rclcpp::Publisher<applanix_msgs::msg::EventGroup>;
  auto group31_pub = std::dynamic_pointer_cast<Group31Publisher>(publishers_[k_topic_event_4]);
  group31_pub->publish(event);
}

void LvClientRos::publishGroup32Callback(const applanix_driver::icd::group::GroupParser &group_parser) {
  using applanix_driver::icd::group::Group32;
  auto event = toRosMessage(group_parser.as<Group32>());
  event.header.frame_id = child_frame_id_;
  if (time_source_ == util::RosTimeSource::NOW) {
    event.header.stamp = ros_clock_.now();
  }
  using Group32Publisher = rclcpp::Publisher<applanix_msgs::msg::EventGroup>;
  auto group32_pub = std::dynamic_pointer_cast<Group32Publisher>(publishers_[k_topic_event_5]);
  group32_pub->publish(event);
}

void LvClientRos::publishGroup33Callback(const applanix_driver::icd::group::GroupParser &group_parser) {
  using applanix_driver::icd::group::Group33;
  auto event = toRosMessage(group_parser.as<Group33>());
  event.header.frame_id = child_frame_id_;
  if (time_source_ == util::RosTimeSource::NOW) {
    event.header.stamp = ros_clock_.now();
  }
  using Group33Publisher = rclcpp::Publisher<applanix_msgs::msg::EventGroup>;
  auto group33_pub = std::dynamic_pointer_cast<Group33Publisher>(publishers_[k_topic_event_6]);
  group33_pub->publish(event);
}

void LvClientRos::publishMessage20Callback(const applanix_driver::icd::message::MessageParser &message_parser) {
  geometry_msgs::msg::TransformStamped ref_to_imu, ref_to_vehicle;
  ref_to_imu.header.frame_id = child_frame_id_;
  // Don't need to use GPS time, this transform is constant.
  ref_to_imu.header.stamp = ros_clock_.now();

  ref_to_vehicle.header = ref_to_imu.header;
  ref_to_vehicle.child_frame_id = frame_id_vehicle_;

  auto params = message_parser.getGeneralParams();
  ref_to_imu.transform.translation = toVector(params.lever_arms.reference_to_imu);
  ref_to_vehicle.transform.translation.x = 0;
  ref_to_vehicle.transform.translation.y = 0;
  ref_to_vehicle.transform.translation.z = 0;

  tf2::Quaternion q;
  q.setRPY(static_cast<double>(params.mounting_angles.reference_to_imu.x),
           static_cast<double>(params.mounting_angles.reference_to_imu.y),
           static_cast<double>(params.mounting_angles.reference_to_imu.z));
  ref_to_imu.transform.rotation = tf2::toMsg(q);
  static_tf_broadcaster_->sendTransform(ref_to_imu);

  q.setRPY(static_cast<double>(params.mounting_angles.reference_to_vehicle.x),
           static_cast<double>(params.mounting_angles.reference_to_vehicle.y),
           static_cast<double>(params.mounting_angles.reference_to_vehicle.z));
  ref_to_vehicle.transform.rotation = tf2::toMsg(q);
  static_tf_broadcaster_->sendTransform(ref_to_vehicle);
}

template<typename ServiceType>
void LvClientRos::createService(const std::string &service_name,
                                 ServiceCallback<ServiceType> service_callback) {
  auto lambda_bound_to_this = [this, service_callback](std::shared_ptr<rmw_request_id_t> request_header,
                                                       typename ServiceType::Request::SharedPtr request,
                                                       typename ServiceType::Response::SharedPtr response) -> void {
    (this->*service_callback)(request_header, request, response);
  };

  typename rclcpp::Service<ServiceType>::SharedPtr
      service = this->create_service<ServiceType>(service_name, lambda_bound_to_this);
  services_.emplace_back(service);  // Should auto case up to std::shared_ptr<rclcpp::ServiceBase>
}

void LvClientRos::setOriginCallback(const std::shared_ptr<rmw_request_id_t>,
                                     const applanix_msgs::srv::SetOrigin::Request::SharedPtr request,
                                     applanix_msgs::srv::SetOrigin::Response::SharedPtr) {
  if (local_cartesian_) {
    local_cartesian_->Reset(request->latitude, request->longitude, request->altitude);
  } else {
    local_cartesian_.emplace(request->latitude, request->longitude, request->altitude);
  }

  RCLCPP_INFO(this->get_logger(),
              "NED origin reset to Lat: %f Lon: %f Alt: %f",
              request->latitude,
              request->longitude,
              request->altitude);
}

void LvClientRos::getUtmZoneCallback(const std::shared_ptr<rmw_request_id_t>,
                                      const applanix_msgs::srv::GetUtmZone::Request::SharedPtr,
                                      applanix_msgs::srv::GetUtmZone::Response::SharedPtr response) {
  const auto &lla = vehicle_nav_solution_->lla;

  int zone;
  bool is_northern_hemisphere;
  double x, y;

  GeographicLib::UTMUPS::Forward(lla.latitude, lla.longitude, zone, is_northern_hemisphere, x, y);

  std::stringstream ss;
  ss << GeographicLib::UTMUPS::EncodeZone(zone, is_northern_hemisphere)
     << ' ' << static_cast<int>(x) << ' ' << static_cast<int>(y);

  response->zone = ss.str();
}

void LvClientRos::getMgrsZoneCallback(const std::shared_ptr<rmw_request_id_t>,
                                       const applanix_msgs::srv::GetMgrsZone::Request::SharedPtr,
                                       applanix_msgs::srv::GetMgrsZone::Response::SharedPtr response) {
  const auto &lla = vehicle_nav_solution_->lla;

  int zone;
  bool is_northern_hemisphere;
  double x, y;

  GeographicLib::UTMUPS::Forward(lla.latitude, lla.longitude, zone, is_northern_hemisphere, x, y);

  std::string s;
  GeographicLib::MGRS::Forward(zone, is_northern_hemisphere, x, y, 5, s);
  response->zone = s;
}

}  // namespace applanix_driver_ros


#include <rclcpp_components/register_node_macro.hpp>
RCLCPP_COMPONENTS_REGISTER_NODE(applanix_driver_ros::LvClientRos)
