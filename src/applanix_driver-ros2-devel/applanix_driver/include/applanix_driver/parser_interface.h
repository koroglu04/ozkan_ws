#pragma once

#include <cstddef>
#include <cstdint>

namespace applanix_driver {
class ParserInterface {
 public:
  virtual void setData(const std::byte *data, std::size_t length) = 0;
  [[nodiscard]] virtual bool isValid() const = 0;
  [[nodiscard]] virtual bool isSupported() const = 0;

 protected:
  const std::byte *data_;
  std::size_t length_;
};
}  // namespace applanix_driver
