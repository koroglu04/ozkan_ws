/**
 * This file defines raw structures allowing fast deserialization of buffers into IDC groups.
 * They differ from the ROS generated classes in that they are byte aligned. Only use these
 * structures for type punning byte arrays. The only exception is Group 3 which contains a
 * variable size part.
 */
#pragma once

#include <byteswap.h>
#include <array>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <string>
#include <set>
#include <vector>

#include "applanix_driver/math.h"

namespace applanix_driver::icd::group {

static const std::vector<std::byte> START = {std::byte{'$'}, std::byte{'G'}, std::byte{'R'}, std::byte{'P'}};
static const std::vector<std::byte> END = {std::byte{'$'}, std::byte{'#'}};

#pragma pack(push, 1)
// @formatter off
// If you add support for a new Group message, don't forget to add it to GroupParser::supported_groups_ and enum
// class SupportedGroups
using Id = std::uint16_t;
static constexpr Id VEHICLE_NAV_SOLUTION = 1;
static constexpr Id VEHICLE_NAV_RMS = 2;
static constexpr Id PRIMARY_GNSS_STATUS = 3;
static constexpr Id TIME_TAGGED_IMU_DATA = 4;
static constexpr Id EVENT_1 = 5;
static constexpr Id EVENT_2 = 6;
static constexpr Id EVENT_3 = 30;
static constexpr Id EVENT_4 = 31;
static constexpr Id EVENT_5 = 32;
static constexpr Id EVENT_6 = 33;
static constexpr Id REFERENCE_NAV_SOLUTION = 401;
static constexpr Id REFERENCE_NAV_RMS = 402;
static constexpr Id GROUP_ID_1 = 1;
static constexpr Id GROUP_ID_2 = 2;
static constexpr Id GROUP_ID_3 = 3;
static constexpr Id GROUP_ID_4 = 4;
static constexpr Id GROUP_ID_5 = 5;
static constexpr Id GROUP_ID_6 = 6;
static constexpr Id GROUP_ID_30 = 30;
static constexpr Id GROUP_ID_31 = 31;
static constexpr Id GROUP_ID_32 = 32;
static constexpr Id GROUP_ID_33 = 33;
static constexpr Id GROUP_ID_401 = 401;
static constexpr Id GROUP_ID_402 = 402;
// @formatter on

struct Header {
  Id id;
  uint16_t byte_count;

  static constexpr size_t OFFSET = 4;
};

struct TimeDistance {
  double time1;
  double time2;
  double distance_tag;
  std::uint8_t time_type;
  std::uint8_t distance_type;

  static constexpr std::uint8_t TIME_1_MASK = 0x0F;
  static constexpr std::uint8_t TIME_2_MASK = 0xF0;
  enum class TimeSource { POS, GPS, UTC, USER, UNKNOWN };

  [[nodiscard]] TimeSource getTime1Source() const {
    return toTimeSource(time_type & TIME_1_MASK);
  }

  [[nodiscard]] TimeSource getTime2Source() const {
    return toTimeSource((time_type & TIME_2_MASK) >> 4);
  }

 private:
  static TimeSource toTimeSource(std::uint8_t type) {
    switch (type) {
      case 0:return TimeSource::POS;
      case 1:return TimeSource::GPS;
      case 2:return TimeSource::UTC;
      case 3:return TimeSource::USER;
      default:return TimeSource::UNKNOWN;
    }
  }
};

struct NavSolution {
  Header header;
  TimeDistance time_distance;
  Llad lla;
  Nedf velocity;
  Rphd attitude;
  double wander_angle;
  float track_angle;
  float speed;
  Xyzf angular_rate;  // x: longitudinal, y: transverse, z: down
  Xyzf acceleration;
  std::int8_t alignment_status;
  std::int8_t padding;
  std::uint16_t checksum;
};

struct Group2 {
  Header header;
  TimeDistance time_distance;
  Nedf position_rms;
  Nedf velocity_rms;
  Rphf attitude_rms;
  float error_ellipsoid_semi_major;
  float error_ellipsoid_semi_minor;
  float error_ellipsoid_orientation;
  std::int8_t padding[2];
  std::uint16_t checksum;
};

enum class GnssConstellation {
  GPS, GLONASS, BEIDOU, SBAS, QZSS, GALILEO, UNKNOWN
};

struct ChannelStatus {
  std::uint16_t sv_prn;
  std::uint16_t channel_tracking_status;
  float azimuth;
  float elevation;
  float sv_l1_snr;
  float sv_l2_snr;

  GnssConstellation getConstellation() const {
    // See PUBS-ICD-003759
    if (sv_prn >= 1 && sv_prn <= 37) return GnssConstellation::GPS;
    else if (sv_prn >= 52 && sv_prn <= 75) return GnssConstellation::GLONASS;
    else if (sv_prn >= 76 && sv_prn <= 105) return GnssConstellation::BEIDOU;
    else if (sv_prn >= 120 && sv_prn <= 138) return GnssConstellation::SBAS;
    else if (sv_prn >= 193 && sv_prn <= 197) return GnssConstellation::QZSS;
    else if (sv_prn >= 201 && sv_prn <= 252) return GnssConstellation::GALILEO;
    else
      return GnssConstellation::UNKNOWN;
  }
};

static constexpr int8_t NAV_STATUS_UNKNOWN = -1;
static constexpr int8_t NAV_STATUS_NO_DATA = 0;
static constexpr int8_t NAV_STATUS_HORZ_CA = 1;
static constexpr int8_t NAV_STATUS_3D_CA = 2;
static constexpr int8_t NAV_STATUS_HORZ_DGPS = 3;
static constexpr int8_t NAV_STATUS_3D_DGPS = 4;
static constexpr int8_t NAV_STATUS_FLOAT_RTK = 5;
static constexpr int8_t NAV_STATUS_WIDE_LANE_RTK = 6;
static constexpr int8_t NAV_STATUS_NARROW_LANE_RTK = 7;
static constexpr int8_t NAV_STATUS_PCODE = 8;
static constexpr int8_t NAV_STATUS_OMNISTAR_HP = 9;
static constexpr int8_t NAV_STATUS_OMNISTAR_HPXP = 10;
static constexpr int8_t NAV_STATUS_OMNISTAR_HPG2 = 11;
static constexpr int8_t NAV_STATUS_OMNISTAR_XP = 12;
static constexpr int8_t NAV_STATUS_OMNISTAR_VBS = 13;
// 14 - no val
static constexpr int8_t NAV_STATUS_OMNISTAR_G2 = 15;
static constexpr int8_t NAV_STATUS_TRIMBLE_RTX = 16;
static constexpr int8_t NAV_STATUS_OMNISTAR_G4 = 17;
static constexpr int8_t NAV_STATUS_OMNISTAR_G2_PLUS = 18;
static constexpr int8_t NAV_STATUS_OMNISTAR_G4_PLUS = 19;

struct Group3 {
  friend class GroupParser;
  Header header;
  TimeDistance time_distance;
  std::int8_t nav_solution_status;
  std::int8_t num_of_sv_tracked;
  std::uint16_t channel_status_byte_count;
  std::vector<ChannelStatus> channel_status;
  float HDOP;
  float VDOP;
  float dgps_correction_latency;
  std::uint16_t dgps_reference_id;
  std::uint32_t gps_week;
  double utc_time_offset;
  float gnss_nav_msg_latency;
  float geoidal_separation;
  std::uint16_t gns_srcv_type;
  std::uint32_t gnss_status;
  std::int8_t padding[2];
  std::uint16_t checksum;

 private:
  // This struct is here for type punning convenience of the non-variable length data
  struct ConstantHeader {
    Header header;
    TimeDistance time_distance;
    std::int8_t nav_solution_status;
    std::int8_t num_of_sv_tracked;
    std::uint16_t channel_status_byte_count;
  };

  // This struct is here for type punning convenience of the non-variable length data
  struct ConstantFooter {
    float HDOP;
    float VDOP;
    float dgps_correction_latency;
    std::uint16_t dgps_reference_id;
    std::uint32_t gps_week;
    double utc_time_offset;
    float gnss_nav_msg_latency;
    float geoidal_separation;
    std::uint16_t gns_srcv_type;
    std::uint32_t gnss_status;
    std::int8_t pad[2];
    std::uint16_t checksum;
  };

  void fillHeader(const ConstantHeader &constant_header);
  void fillFooter(const ConstantFooter &constant_footer);
};
using PrimaryGnssStatus = Group3;

struct Group4 {
  Header header;
  TimeDistance time_distance;
  std::array<std::byte, 24> imu_data;
  std::byte data_status;
  std::uint8_t imu_type;
  std::byte data_rate;
  std::uint16_t imu_status;
  std::byte padding;
  std::uint16_t checksum;
};
using TimeTaggedImuData = Group4;

struct Event {
  Header header;
  TimeDistance time_distance;
  std::uint32_t event_pulse_number;
  std::uint16_t padding;
  std::uint16_t checksum;
};
using Group5 = Event;
using Group6 = Event;
using Group30 = Event;
using Group31 = Event;
using Group32 = Event;
using Group33 = Event;
using Event1 = Group5;
using Event2 = Group6;
using Event3 = Group30;
using Event4 = Group31;
using Event5 = Group32;
using Event6 = Group33;

static const std::vector<std::byte> IMU_DATA_HEADER = {
    std::byte{'$'}, std::byte{'I'}, std::byte{'M'}, std::byte{'U'}
};

struct Group100002 {
  friend class GroupParser;

  Header header;
  TimeDistance time_distance;
  std::array<std::byte, 6> imu_header;  // $IMUnn where nn identifies the IMU type
  std::uint16_t imu_byte_count;
  std::vector<std::byte> imu_raw_data;
  std::uint16_t data_checksum;  // Checksum of IMU data
  // 0 padding
  std::uint16_t checksum;

  [[nodiscard]] unsigned int getImuType() const {
    char str[2];
    std::size_t nn_idx = IMU_DATA_HEADER.size();
    str[0] = static_cast<char>(imu_header[nn_idx]);
    str[1] = static_cast<char>(imu_header[nn_idx + 1]);
    return std::stoul(str, nullptr, 10);
  }

 private:
  struct ConstantHeader {
    Header header;
    TimeDistance time_distance;
    std::array<std::byte, 6> imu_header;
    std::uint16_t imu_byte_count;
  };

  struct ConstantFooter {
    std::uint16_t data_checksum;  // Checksum of IMU data
    std::uint16_t checksum;
  };

  void fillHeader(const ConstantHeader &constant_header);
  void fillFooter(const ConstantFooter &constant_footer);
};
using RawImuData = Group100002;

using Group401 = NavSolution;
using Group402 = Group2;

// Using definitions for more verbose group naming
using NavSolution = NavSolution;
using NavRms = Group2;
using VehicleNavSolution = NavSolution;
using VehicleNavRms = NavRms;
using ReferenceNavSolution = Group401;
using ReferenceNavRms = Group402;

#pragma pack(pop)

}  // namespace applanix_driver::icd::group
