#pragma once

#include <cstring>
#include <set>
#include "applanix_driver/icd/message.h"
#include "applanix_driver/icd/icd_parser_interface.h"

namespace applanix_driver::icd::message {

class MessageParser : public IcdParserInterface {
 public:
  MessageParser(const std::byte *data, std::size_t length);

  void setData(const std::byte *data, std::size_t length) override;
  [[nodiscard]] bool isValid() const override;
  [[nodiscard]] bool isSupported() const override;

  [[nodiscard]] Id getId() const;
  [[nodiscard]] Header getHeader() const;
  [[nodiscard]] GeneralParams getGeneralParams() const;

  [[nodiscard]] std::size_t getStartBytesSize() const override;
 private:
  template<typename T>
  [[nodiscard]] T typeCastMessage() const {
    T group;
    std::memcpy(&group, data_ + Header::OFFSET, sizeof(T));
    return group;
  }

  static const std::set<Id> supported_messages_;
};
}  // namespace applanix_driver::icd::message
