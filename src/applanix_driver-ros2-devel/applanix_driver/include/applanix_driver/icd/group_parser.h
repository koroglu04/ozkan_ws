#pragma once
#include <cstdint>
#include <cstring>
#include <set>
#include "applanix_driver/icd/group.h"
#include "applanix_driver/icd/icd_parser_interface.h"

namespace applanix_driver::icd::group {
class GroupParser : public IcdParserInterface {
 public:
  GroupParser(const std::byte *data, size_t length);
  GroupParser(const GroupParser &rhs);
  GroupParser &operator=(const GroupParser &rhs) = default;

  void setData(const std::byte *data, std::size_t length) override;

  [[nodiscard]] Id getId() const;
  [[nodiscard]] Header getHeader() const;

  /**
   * Main parsing method that allows getting a certain group. For non-constant size groups, this method has to be
   * template specialized with parsing done manually, otherwise simple type punning can be performed.
   * @tparam T
   * @return
   */
  template<typename T>
  [[nodiscard]] T as() const {
    return typeCastGroup<T>();
  }

  [[nodiscard]] bool isValid() const override;
  [[nodiscard]] bool isSupported() const override;

  [[nodiscard]] std::size_t getStartBytesSize() const override;
 private:
  using GroupIdSet = std::set<Id>;
  static const GroupIdSet supported_groups_;
  static const std::size_t GROUP_DATA_OFFSET;

  const std::byte *data_;
  std::size_t length_;

  /**
 * Type casts a group using memory starting right after the group start bytes "$GRP"
 * @tparam T
 * @return
 */
  template<typename T>
  [[nodiscard]] T typeCastGroup() const {
    T group;
    memcpy(&group, data_ + Header::OFFSET, sizeof(T));
    return group;
  }
};

template<>
[[nodiscard]] inline Group3 GroupParser::as<Group3>() const {
  // Variable length group, do not type pun
  Group3 group;

  auto header = typeCastGroup<Group3::ConstantHeader>();
  size_t num_channel_status = header.channel_status_byte_count / sizeof(ChannelStatus);
  const std::byte *channel_status = data_ + Header::OFFSET + sizeof(Group3::ConstantHeader);
  for (size_t i = 0; i < num_channel_status; ++i) {
    group.channel_status.push_back(typePun<ChannelStatus>(channel_status));
    channel_status += sizeof(ChannelStatus);
  }

  group.fillHeader(header);

  // Channel status pointer is now past the end of channel statuses
  Group3::ConstantFooter footer;
  memcpy(&footer, channel_status, sizeof(Group3::ConstantFooter));

  group.fillFooter(footer);

  return group;
}

template<>
[[nodiscard]] inline Group100002 GroupParser::as<Group100002>() const {
  Group100002 group;

  auto header = typeCastGroup<Group100002::ConstantHeader>();
  group.fillHeader(header);

  const std::byte* imu_data = static_cast<const std::byte*>(data_) +
      Header::OFFSET + sizeof(Group100002::ConstantHeader);
  group.imu_raw_data = std::vector<std::byte>(imu_data, imu_data + group.imu_byte_count);

  Group100002::ConstantFooter footer;
  std::memcpy(&footer, imu_data + group.imu_byte_count, sizeof(Group100002::ConstantFooter));

  group.fillFooter(footer);

  return group;
}

}  // namespace applanix_driver::icd::group
