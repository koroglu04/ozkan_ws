#pragma once

#include "applanix_driver/parser_interface.h"

namespace applanix_driver::icd {

class IcdParserInterface : public ParserInterface {
 public:
 protected:
  /**
   * Perform type punning on an area of memory
   * @tparam T      Type to convert to
   * @param data
   * @return
   */
  template<typename T>
  [[nodiscard]] T typePun(const std::byte *data) const {
    T val;
    memcpy(&val, data, sizeof(T));
    return val;
  }

  /**
   * Get the size of the the bytes are are always in the ICD data i.e. STX and ETX sizes;
   * @return
   */
  [[nodiscard]] virtual std::size_t getStartBytesSize() const = 0;

 private:
};

}  // namespace applanix_driver::icd
