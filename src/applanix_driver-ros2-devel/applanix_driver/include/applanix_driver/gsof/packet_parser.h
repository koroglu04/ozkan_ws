#pragma once

#include <cstdint>

#include "applanix_driver/gsof/message_parser.h"
#include "applanix_driver/parser_interface.h"

namespace applanix_driver::gsof {
class PacketParser : public ParserInterface {
 public:
  PacketParser();
  /**
   * @brief The PacketParser class expects to be parsing a report containing one or more GSOF
   * messages.
   * 
   * @param data Pointer to the start of the report (i.e. contains START_TX byte and page numbers)
   * @param length
   */
  PacketParser(const std::byte *data, std::size_t length);

  void setData(const std::byte *data, std::size_t length) override;
  [[nodiscard]] bool isValid() const override;
  [[nodiscard]] bool isSupported() const override;

  [[nodiscard]] MessageParser getMessageParser() const;

 private:
  const std::byte *messages_;

  MessageParser message_parser_;
};
}  // namespace applanix_driver::gsof
