#pragma once

#include <optional>

#include <applanix_msgs/srv/set_origin.hpp>
#include <applanix_msgs/srv/get_utm_zone.hpp>
#include <applanix_msgs/srv/get_mgrs_zone.hpp>
#include <rclcpp/rclcpp.hpp>
#include <GeographicLib/LocalCartesian.hpp>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/static_transform_broadcaster.h>

#include "applanix_driver/lv_client.h"
#include "applanix_driver/icd/group_parser.h"
#include "util/ros_time_source.h"

namespace applanix_driver_ros {

class LvClientRos : public rclcpp::Node {
 public:
  LvClientRos(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

 private:
  static constexpr char k_default_node_name[] = "lv_client";
  static constexpr char k_topic_odometry[] = "~/odom";
  static constexpr char k_topic_utm[] = "~/utm";
  static constexpr char k_topic_navsat[] = "~/navsat";
  static constexpr char k_topic_event_1[] = "~/event_1";
  static constexpr char k_topic_event_2[] = "~/event_2";
  static constexpr char k_topic_event_3[] = "~/event_3";
  static constexpr char k_topic_event_4[] = "~/event_4";
  static constexpr char k_topic_event_5[] = "~/event_5";
  static constexpr char k_topic_event_6[] = "~/event_6";
  static constexpr char k_topic_group_1[] = "~/group/navigation_solution_1";
  static constexpr char k_topic_group_2[] = "~/group/navigation_performance_2";
  static constexpr char k_topic_group_3[] = "~/group/primary_gnss_status_3";
  static constexpr char k_topic_group_4[] = "~/group/time_tagged_imu_data_4";
  static constexpr char k_topic_group_5[] = "~/group/event_1_5";
  static constexpr char k_topic_group_6[] = "~/group/event_2_6";
  static constexpr char k_topic_group_30[] = "~/group/event_3_30";
  static constexpr char k_topic_group_31[] = "~/group/event_4_31";
  static constexpr char k_topic_group_32[] = "~/group/event_5_32";
  static constexpr char k_topic_group_33[] = "~/group/event_6_33";
  static constexpr char k_topic_group_401[] = "~/group/reference_navigation_solution_401";
  static constexpr char k_topic_group_402[] = "~/group/reference_navigation_performance_402";
  static constexpr char k_service_set_origin[] = "~/set_origin";
  static constexpr char k_service_get_utm_zone[] = "~/get_utm_zone";
  static constexpr char k_service_get_mgrs_zone[] = "~/get_mgrs_zone";
  static constexpr int k_default_qos_history_depth = 100;

  std::string parent_frame_id_;
  std::string child_frame_id_;
  std::string parent_frame_utm_id_;
  std::string child_frame_utm_id_;
  std::string frame_id_vehicle_;

  std::optional<applanix_driver::LvClient> lv_client_;

  util::RosTimeSource time_source_;
  rclcpp::Clock ros_clock_;
  bool publish_tf_;
  bool publish_tf_utm_;
  bool publish_icd_msgs_;
  bool publish_ros_msgs_;

  std::unordered_map<std::string, std::shared_ptr<rclcpp::PublisherBase>> publishers_;
  std::vector<std::shared_ptr<rclcpp::ServiceBase>> services_;
  std::optional<tf2_ros::TransformBroadcaster> transform_broadcaster_;
  std::optional<tf2_ros::StaticTransformBroadcaster> static_tf_broadcaster_;

  std::optional<applanix_driver::icd::group::VehicleNavSolution> vehicle_nav_solution_;
  std::optional<applanix_driver::icd::group::VehicleNavRms> vehicle_nav_rms_;
  std::optional<applanix_driver::icd::group::PrimaryGnssStatus> primary_gnss_status_;

  std::optional<GeographicLib::LocalCartesian> local_cartesian_;
  std::mutex mtx_local_origin_;

  using GroupCallback = void (LvClientRos::*)(const applanix_driver::icd::group::GroupParser &);
  using MessageCallback = void (LvClientRos::*)(const applanix_driver::icd::message::MessageParser &);

  template<typename RosMessageType>
  void registerAndAdvertise(applanix_driver::icd::group::Id group_id, GroupCallback callback, const std::string &topic);
  void registerCallback(applanix_driver::icd::group::Id group_id, GroupCallback callback);
  void registerCallback(applanix_driver::icd::message::Id message_id, MessageCallback callback);

  using GroupParser = applanix_driver::icd::group::GroupParser;

  // Callbacks to save data in private members
  void saveGroup1Callback(const GroupParser &group_parser);
  void saveGroup2Callback(const GroupParser &group_parser);
  void saveGroup3Callback(const GroupParser &group_parser);

  // Calbacks to publish data in "standard" ROS messages
  void publishOdometryAndNavSatCallback(const GroupParser &group_parser);

  // Callbacks to publish data in custom applanix ICD ROS messages
  void publishGroup1Callback(const GroupParser &group_parser);
  void publishGroup2Callback(const GroupParser &group_parser);
  void publishGroup4Callback(const GroupParser &group_parser);
  void publishGroup5Callback(const GroupParser &group_parser);
  void publishGroup6Callback(const GroupParser &group_parser);
  void publishGroup30Callback(const GroupParser &group_parser);
  void publishGroup31Callback(const GroupParser &group_parser);
  void publishGroup32Callback(const GroupParser &group_parser);
  void publishGroup33Callback(const GroupParser &group_parser);

  // Callbacks to publish data from ICD messages
  void publishMessage20Callback(const applanix_driver::icd::message::MessageParser &message_parser);

  template<typename ServiceType>
  using ServiceCallback = void (LvClientRos::*)(const std::shared_ptr<rmw_request_id_t>,
                                                 const typename ServiceType::Request::SharedPtr,
                                                 typename ServiceType::Response::SharedPtr);
  template<typename ServiceType>
  void createService(const std::string &service_name, ServiceCallback<ServiceType> service_callback);
  void setOriginCallback(const std::shared_ptr<rmw_request_id_t> request_header,
                         const applanix_msgs::srv::SetOrigin::Request::SharedPtr request,
                         applanix_msgs::srv::SetOrigin::Response::SharedPtr response);
  void getUtmZoneCallback(const std::shared_ptr<rmw_request_id_t> request_header,
                          const applanix_msgs::srv::GetUtmZone::Request::SharedPtr request,
                          applanix_msgs::srv::GetUtmZone::Response::SharedPtr response);
  void getMgrsZoneCallback(const std::shared_ptr<rmw_request_id_t> request_header,
                           const applanix_msgs::srv::GetMgrsZone::Request::SharedPtr request,
                           applanix_msgs::srv::GetMgrsZone::Response::SharedPtr response);

};

}  // namespace applanix_driver_ros
