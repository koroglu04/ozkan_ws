#pragma once

#include <array>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <string>
#include <vector>
#include <boost/asio.hpp>

#include "network/ip_client.h"

namespace network {
class TcpClientDelimiter : public IpClient {
 public:
  TcpClientDelimiter() = delete;
  /**
   * A thin wrapper over a TCP socket which parses the stream using the given delimeters
   */
  TcpClientDelimiter(const std::string &ip_address, unsigned int port,
                     std::vector<std::byte> start_delimiter,
                     std::vector<std::byte> end_delimiter) noexcept;

  util::Status open() override;
  int receive() override;

 private:
  std::string start_delimiter_;
  std::string end_delimiter_;

  boost::asio::io_service io_service_;
  boost::asio::basic_streambuf<std::allocator<std::uint8_t>> streambuf_;
  std::unique_ptr<boost::asio::ip::tcp::socket> socket_;
};
}  // namespace network
