#pragma once

#include <memory>
#include <string>
#include <boost/asio.hpp>

#include "network/ip_client.h"

namespace network {

/**
 * A thin wrapper around a tcp socket
 */
class TcpClient : public IpClient {
 public:
  TcpClient() = delete;
  TcpClient(const std::string& ip_address, unsigned int port);

  util::Status open() override;
  int receive() override;

 private:
  boost::asio::io_service io_service_;
  std::unique_ptr<boost::asio::ip::tcp::socket> socket_;

};

}  // namespace network
