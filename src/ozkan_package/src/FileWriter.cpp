//
// Created by ozkan on 15.02.2021.
//

#include "FileWriter.h"

FileWriter::FileWriter() {

}

FileWriter::~FileWriter() {

}
void FileWriter::openFile(std::ofstream *F, const char * str){
    F->open(str);
}

void FileWriter::writeToFile(std::ofstream *F, log_data_typedef *P) {

    *F << P->Roll << ",";
    *F << P->Pitch << ",";
    *F << P->Heading << ",";
    *F << P->TrackAngle << ",";
    *F << P->Latitude << ",";
    *F << P->Longitude << ",";
    *F << P->Altitude << ",";
    *F << P->WheelSpeedLeft << ",";
    *F << P->WheelSpeedRight << ",";
    *F << P->SteeringAngle << ",";
    *F << P->Battery.Voltage << ",";
    *F << P->Battery.Current << ",";
    *F << P->Battery.Consupmtion << ",";
    *F << P->MotorTorque << ",";
    *F << (int)P->VehicleSpeed << ",";
    *F << P->NorthRmsErr << ",";
    *F << P->EastRmsErr << ",";
    *F << std::endl;
}

void FileWriter::writeTitles(std::ofstream *F, const char **s) {
    for (int i = 0; i < 17; i++){
        *F << s[i] << ",";
    }
    *F << std::endl;
}

