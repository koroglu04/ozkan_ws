#include "ozkan_package.h"

OzkanNode::OzkanNode() :  Node("ozkan_node"), counter(0){


    publisher_ = this->create_publisher<std_msgs::msg::String>("topic", 10);

    sub_msg_49_ = this->create_subscription<applanix_msgs::msg::NavigationSolutionGsof49>(
            "/lvx_client/gsof/ins_solution_49", rclcpp::QoS(10),
            std::bind(&OzkanNode::msg49Callback, this, std::placeholders::_1));

    sub_msg_50_ = this->create_subscription<applanix_msgs::msg::NavigationPerformanceGsof50>(
            "/lvx_client/gsof/ins_solution_rms_50", rclcpp::QoS(10),
            std::bind(&OzkanNode::msg50Callback, this, std::placeholders::_1));

    sub_wheel_speed_left_ = this->create_subscription<std_msgs::msg::Float32>(
            "/CAN/WheelSpeed_Left", rclcpp::QoS(10),
            std::bind(&OzkanNode::msgWhlSpdLeftCallback, this, std::placeholders::_1));

    sub_wheel_speed_right_ = this->create_subscription<std_msgs::msg::Float32>(
            "/CAN/WheelSpeed_Right", rclcpp::QoS(10),
            std::bind(&OzkanNode::msgWhlSpdRighCallback, this, std::placeholders::_1));

    sub_steering_angle_ = this->create_subscription<std_msgs::msg::Float32>(
            "/CAN/Steering_Angle", rclcpp::QoS(10),
            std::bind(&OzkanNode::msgSteerAngleCallback, this, std::placeholders::_1));

    sub_bat_volt_ = this->create_subscription<std_msgs::msg::Float32>(
            "/CAN/BatteryVoltage", rclcpp::QoS(10),
            std::bind(&OzkanNode::msgBatVoltCallback, this, std::placeholders::_1));

    sub_bat_cur_= this->create_subscription<std_msgs::msg::Float32>(
            "/CAN/BatteryCurrent", rclcpp::QoS(10),
            std::bind(&OzkanNode::msgBatCurCallback, this, std::placeholders::_1));

    sub_bat_cons_ = this->create_subscription<std_msgs::msg::Float32>(
            "/CAN/BatteryConsumption", rclcpp::QoS(10),
            std::bind(&OzkanNode::msgBatConsumpCallback, this, std::placeholders::_1));

    sub_motor_torque_ = this->create_subscription<std_msgs::msg::Float32>(
            "/CAN/MotorTorque", rclcpp::QoS(10),
            std::bind(&OzkanNode::msgMotorTorqueCallback, this, std::placeholders::_1));

    sub_veh_vel_ = this->create_subscription<std_msgs::msg::UInt8>(
            "/CAN/VehicleSpeed", rclcpp::QoS(10),
            std::bind(&OzkanNode::msgVehicleVelCallback, this, std::placeholders::_1));

    timer_ = this->create_wall_timer(
            50ms, std::bind(&OzkanNode::timer_callback, this));

    std::time_t t = std::time(0);   // get time now
    std::tm* now = std::localtime(&t);

    std::string file_str;
    std::string file_name;
    std::string file_base_name = "Log";
    std::string file_type = ".csv";
    std::string file_path = "/home/ozkan/Documents/AESK_Logs/";
    std::string year_str = std::to_string(now->tm_year + 1900);
    std::string mon_str = std::to_string((now->tm_mon + 1) );
    std::string day_str = std::to_string(now->tm_mday);
    std::string hour_str = std::to_string(now->tm_hour);
    std::string min_str = std::to_string(now->tm_min);
    std::string sec_str = std::to_string(now->tm_sec);

    file_name = file_base_name + "-" +
                year_str + "-" +
                mon_str + "-" +
                day_str + "-" +
                hour_str + "-" +
                min_str + "-" +
                sec_str + file_type;

    file_str = file_path + file_name;

    FileWriter::openFile(&logFile,file_str.c_str());

    const char *TitleArray[17] = {
            "Roll",
            "Pitch",
            "Heading",
            "TrackAngle",
            "Latitude",
            "Longitude",
            "Altitude",
            "WheelSpeedLeft",
            "WheelSpeedRight",
            "SteeringAngle",
            "BatteryVoltage",
            "BatteryCurrent",
            "BatteryConsump",
            "MotorTorque",
            "VehicleVelocity",
            "NorthRmsErr",
            "EastRmsErr"
    };
    FileWriter::writeTitles(&logFile, TitleArray);
}

OzkanNode::~OzkanNode() {
    logFile.close();
}

void OzkanNode::timer_callback() {

    FileWriter::writeToFile(&logFile, &log_data);

    std::cout << "Log Counter : " << ++counter <<
                ", " << Msg49CallbackFlag <<
                ", " << Msg50CallbackFlag <<
                ", " << WheelSpeedLeftMsgFlag <<
                ", " << WheelSpeedRightMsgFlag <<
                ", " << SteeringAngleMsgFlag <<
                ", " << BatVoltMsgFlag <<
                ", " << BatCurMsgFlag <<
                ", " << BatConsMsgFlag <<
                ", " << MotorTorqueMsgFlag <<
                ", " << VehVelMsgFlag << std::endl;

    Msg49CallbackFlag = false;
    Msg50CallbackFlag  = false;
    WheelSpeedLeftMsgFlag = false;
    WheelSpeedRightMsgFlag = false;
    SteeringAngleMsgFlag = false;
    BatVoltMsgFlag = false;
    BatCurMsgFlag = false;
    BatConsMsgFlag = false;
    MotorTorqueMsgFlag = false;
    VehVelMsgFlag = false;

}

void OzkanNode::msg49Callback(const applanix_msgs::msg::NavigationSolutionGsof49::SharedPtr Msg) {
    //std::cout << "In msg 49 Callback!!" << std::endl;
    Msg49CallbackFlag = true;
    log_data.Roll = Msg->roll;
    log_data.Pitch = Msg->pitch;
    log_data.Heading = Msg->heading;
    log_data.TrackAngle = Msg->track_angle;
    log_data.Latitude = Msg->lla.latitude;
    log_data.Longitude = Msg->lla.longitude;
    log_data.Altitude = Msg->lla.altitude;
}

void OzkanNode::msg50Callback(const applanix_msgs::msg::NavigationPerformanceGsof50::SharedPtr Msg) {
    // std::cout << "In msg 50 Callback!!" << std::endl;
    Msg50CallbackFlag = true;
    log_data.NorthRmsErr = Msg->pos_rms_error.north;
    log_data.EastRmsErr = Msg->pos_rms_error.east;
    //std::cout << "east_rms : " <<  Msg->pos_rms_error.east << ", north_rms : " << Msg->pos_rms_error.north << std::endl;
}

void OzkanNode::msgWhlSpdLeftCallback(std_msgs::msg::Float32::SharedPtr _msg) {
    WheelSpeedLeftMsgFlag = true;
    log_data.WheelSpeedLeft = _msg->data;
    //std::cout << "In msgWhlSpdLeftCallback" << std::endl;
}

void OzkanNode::msgWhlSpdRighCallback(std_msgs::msg::Float32::SharedPtr _msg) {
    WheelSpeedRightMsgFlag = true;
    log_data.WheelSpeedRight = _msg->data;
    //std::cout << "In msgWhlSpdRighCallback" << std::endl;
}

void OzkanNode::msgSteerAngleCallback(std_msgs::msg::Float32::SharedPtr _msg) {
    SteeringAngleMsgFlag = true;
    log_data.SteeringAngle = _msg->data;
    //std::cout << "In msgSteerAngleCallback" << std::endl;
}

void OzkanNode::msgBatVoltCallback(std_msgs::msg::Float32::SharedPtr _msg) {
    BatVoltMsgFlag = true;
    log_data.Battery.Voltage = _msg->data;
}

void OzkanNode::msgBatCurCallback(std_msgs::msg::Float32::SharedPtr _msg) {
    BatCurMsgFlag = true;
    log_data.Battery.Current = _msg->data;
}

void OzkanNode::msgBatConsumpCallback(std_msgs::msg::Float32::SharedPtr _msg) {
    BatConsMsgFlag = true;
    log_data.Battery.Consupmtion = _msg->data;
}

void OzkanNode::msgMotorTorqueCallback(std_msgs::msg::Float32::SharedPtr _msg) {
    MotorTorqueMsgFlag = true;
    log_data.MotorTorque = _msg->data;
}

void OzkanNode::msgVehicleVelCallback(std_msgs::msg::UInt8::SharedPtr _msg) {
    VehVelMsgFlag = true;
    log_data.VehicleSpeed = _msg->data;
}

