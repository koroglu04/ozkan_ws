//
// Created by ozkan on 11.01.2021.
//

// sudo ip link set can0 type can bitrate 250000
// sudo ip link set can0 up


#include "ozkan_package.h"
#include "FileWriter.h"



int main(int argc, char * argv[]) {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<OzkanNode>());
    rclcpp::shutdown();
    return 0;
}

