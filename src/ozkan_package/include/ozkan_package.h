//
// Created by ozkan on 11.01.2021.
//

#ifndef OZKAN_PACKAGE_OZKAN_PACKAGE_H


#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <atomic>
#include <chrono>
#include <ctime>


#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/float32.hpp"
#include "std_msgs/msg/u_int8.hpp"

#include "FileWriter.h"

#include <applanix_msgs/msg/navigation_solution_gsof49.hpp>
#include <applanix_msgs/msg/navigation_performance_gsof50.hpp>

using namespace std::chrono_literals;

/* This example creates a subclass of Node and uses std::bind() to register a
* member function as a callback from the timer. */

class OzkanNode : public rclcpp::Node, FileWriter {
public:
    OzkanNode();
    ~OzkanNode();

    void timer_callback();





private:
    void msg49Callback(const applanix_msgs::msg::NavigationSolutionGsof49::SharedPtr Msg);
    void msg50Callback(const applanix_msgs::msg::NavigationPerformanceGsof50::SharedPtr Msg);

    void msgWhlSpdLeftCallback(std_msgs::msg::Float32::SharedPtr _msg);
    void msgWhlSpdRighCallback(std_msgs::msg::Float32::SharedPtr _msg);
    void msgSteerAngleCallback(std_msgs::msg::Float32::SharedPtr _msg);
    void msgBatCurCallback(std_msgs::msg::Float32::SharedPtr _msg);
    void msgBatVoltCallback(std_msgs::msg::Float32::SharedPtr _msg);
    void msgBatConsumpCallback(std_msgs::msg::Float32::SharedPtr _msg);
    void msgMotorTorqueCallback(std_msgs::msg::Float32::SharedPtr _msg);
    void msgVehicleVelCallback(std_msgs::msg::UInt8::SharedPtr _msg);

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;

    rclcpp::Subscription<applanix_msgs::msg::NavigationSolutionGsof49>::SharedPtr sub_msg_49_;
    rclcpp::Subscription<applanix_msgs::msg::NavigationPerformanceGsof50>::SharedPtr sub_msg_50_;

    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr sub_wheel_speed_left_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr sub_wheel_speed_right_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr sub_steering_angle_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr sub_bat_volt_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr sub_bat_cur_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr sub_bat_cons_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr sub_motor_torque_;
    rclcpp::Subscription<std_msgs::msg::UInt8>::SharedPtr sub_veh_vel_;

    std::atomic<bool> Msg49CallbackFlag;
    std::atomic<bool> Msg50CallbackFlag;
    std::atomic<bool> WheelSpeedLeftMsgFlag;
    std::atomic<bool> WheelSpeedRightMsgFlag;
    std::atomic<bool> SteeringAngleMsgFlag;
    std::atomic<bool> BatVoltMsgFlag;
    std::atomic<bool> BatCurMsgFlag;
    std::atomic<bool> BatConsMsgFlag;
    std::atomic<bool> MotorTorqueMsgFlag;
    std::atomic<bool> VehVelMsgFlag;

    log_data_typedef log_data;


    unsigned long long counter;

};

#define OZKAN_PACKAGE_OZKAN_PACKAGE_H

#endif //OZKAN_PACKAGE_OZKAN_PACKAGE_H
