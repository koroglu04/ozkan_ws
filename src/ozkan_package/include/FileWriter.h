//
// Created by ozkan on 15.02.2021.
//

#include <fstream>
#include <atomic>

#ifndef OZKAN_PACKAGE_FILEWRITER_H
#define OZKAN_PACKAGE_FILEWRITER_H



class FileWriter {
public:

    FileWriter();
    ~FileWriter();

    std::ofstream logFile;

    typedef struct {
        std::atomic<float> Roll;
        std::atomic<float> Pitch;
        std::atomic<float> Heading;
        std::atomic<float> TrackAngle;
        std::atomic<float> Latitude;
        std::atomic<float> Longitude;
        std::atomic<float> Altitude;
        std::atomic<float> NorthRmsErr;
        std::atomic<float> EastRmsErr;
        std::atomic<float> WheelSpeedLeft;
        std::atomic<float> WheelSpeedRight;
        std::atomic<float> SteeringAngle;
        struct{
            std::atomic<float> Voltage;
            std::atomic<float> Current;
            std::atomic<float> Consupmtion;
        }Battery;
        std::atomic<float> MotorTorque;
        std::atomic<uint8_t> VehicleSpeed;
    }log_data_typedef;

    void openFile(std::ofstream *F, const char * str);
    void writeToFile(std::ofstream *F, log_data_typedef *P);
    void writeTitles(std::ofstream *F, const char **s);

private:


};


#endif //OZKAN_PACKAGE_FILEWRITER_H
