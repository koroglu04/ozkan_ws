#include "../include/second_package/CanReceiver.h"
#include <iostream>


CanReceiver::CanReceiver() : Node("ozkan_node") {


    wheelspeed_left_ = this->create_publisher<std_msgs::msg::Float32>("/CAN/WheelSpeed_Left", 10);
    wheelspeed_right_ = this->create_publisher<std_msgs::msg::Float32>("/CAN/WheelSpeed_Right", 10);
    steer_angle_ = this->create_publisher<std_msgs::msg::Float32>("/CAN/Steering_Angle", 10);
    bat_volt_ = this->create_publisher<std_msgs::msg::Float32>("/CAN/BatteryVoltage", 10);
    bat_cur_ = this->create_publisher<std_msgs::msg::Float32>("/CAN/BatteryCurrent", 10);
    bat_cons_ = this->create_publisher<std_msgs::msg::Float32>("/CAN/BatteryConsumption", 10);
    motor_torque_ = this->create_publisher<std_msgs::msg::Float32>("/CAN/MotorTorque", 10);
    veh_speed_ = this->create_publisher<std_msgs::msg::UInt8>("/CAN/VehicleSpeed", 10);
}

CanReceiver::~CanReceiver() {

}

void CanReceiver::setUp() {
    constexpr auto test_interface = "can0";
    receiver_ = std::make_unique<SocketCanReceiver>(test_interface);
    sender_ = std::make_unique<SocketCanSender>(test_interface);
}

void CanReceiver::receive() {

    uint8_t receive_msg[8];
    CanId receive_id{};
    receive_id = receiver_->receive(receive_msg, receive_timeout_);
/*
    std::cout << receive_id.identifier() << " " << (int)receive_msg[0] << " "
                                                << (int)receive_msg[1] << " "
                                                << (int)receive_msg[2] << " "
                                                << (int)receive_msg[3] << " "
                                                << (int)receive_msg[4] << " "
                                                << (int)receive_msg[5] << " "
                                                << (int)receive_msg[6] << " "
                                                << (int)receive_msg[7] << std::endl;
*/
    if (receive_id.identifier() == 0x1555AAA0){
        /*
              std::cout << receive_id.identifier() << " " << (int)receive_msg[0] << " "
                        << (int)receive_msg[1] << " "
                        << (int)receive_msg[2] << " "
                        << (int)receive_msg[3] << " "
                        << (int)receive_msg[4] << " "
                        << (int)receive_msg[5] << " "
                        << (int)receive_msg[6] << " "
                        << (int)receive_msg[7] << std::endl;

              std::cout << "WhlLef : " << byte_to_i16(receive_msg) <<
                           ", WhlRigh : " << byte_to_i16(receive_msg + 2) <<
                           ", StrAngle : "<< byte_to_i16(receive_msg + 4) << std::endl;
      */
        WheelSpeed.Left = (float)(byte_to_i16(receive_msg)) / 10;
        WheelSpeed.Right = (float)(byte_to_i16(receive_msg + 2)) / 10;
        SteeringAngle = (float)(byte_to_i16(receive_msg + 4));

        std::cout << "L : " << WheelSpeed.Left << ", R : " << WheelSpeed.Right << ", SA : " << SteeringAngle << std::endl;

        std_msgs::msg::Float32 WheelSpeed_Left_Msg;
        std_msgs::msg::Float32 WheelSpeed_Right_Msg;
        std_msgs::msg::Float32 Steering_Angle_Msg;


        WheelSpeed_Left_Msg.data = WheelSpeed.Left;
        WheelSpeed_Right_Msg.data = WheelSpeed.Right;
        Steering_Angle_Msg.data = SteeringAngle;

        wheelspeed_left_->publish(WheelSpeed_Left_Msg);
        wheelspeed_right_->publish(WheelSpeed_Right_Msg);
        steer_angle_->publish(Steering_Angle_Msg);

    }

    else if (receive_id.identifier() == 0x1555BBB0){
        Battery.Voltage = (float)(byte_to_u16(receive_msg)) / 100;
        Battery.Current = (float)(byte_to_i16(receive_msg + 2)) / 100;
        Battery.Consumption = (float)(byte_to_u16(receive_msg + 4)) / 10;

        std_msgs::msg::Float32 bat_vol_msg;
        std_msgs::msg::Float32 bat_cur_msg;
        std_msgs::msg::Float32 bat_cons_msg;

        bat_vol_msg.data = Battery.Voltage;
        bat_cur_msg.data = Battery.Current;
        bat_cons_msg.data = Battery.Consumption;

        bat_volt_->publish(bat_vol_msg);
        bat_cur_->publish(bat_cur_msg);
        bat_cons_->publish(bat_cons_msg);
    }
    else if (receive_id.identifier() == 0x1555DDD1){
        MotorTorque = (float)(byte_to_i16(receive_msg + 6)) / 100;

        std_msgs::msg::Float32 motor_torque_msg;
        motor_torque_msg.data = MotorTorque;
        motor_torque_->publish(motor_torque_msg);
    }
    else if (receive_id.identifier() == 0x1555DDD2){
        VehicleSpeed_kph = receive_msg[7];

        std_msgs::msg::UInt8 vehicle_vel_msg;
        vehicle_vel_msg.data = VehicleSpeed_kph;
        veh_speed_->publish(vehicle_vel_msg);
    }
}

void CanReceiver::send(uint32_t ID, uint8_t DLC, uint8_t *DataP) {

    uint8_t data[8];

    for (int i = 0; i < DLC; i++)
        data[i] = DataP[i];

    static CanId send_id{static_cast<CanId::IdT>(ID), static_cast<CanId::LengthT>(DLC)};

    sender_->send(data, send_id, send_timeout_);

}

uint16_t CanReceiver::byte_to_u16(uint8_t *P){
    union {
        uint16_t u16;
        uint8_t u8[2];
    }data;
    data.u8[0] = P[0];
    data.u8[1] = P[1];
    return data.u16;
}

int16_t CanReceiver::byte_to_i16(uint8_t *P){
    union {
        int16_t i16;
        uint8_t u8[2];
    }data;
    data.u8[0] = P[0];
    data.u8[1] = P[1];
    return data.i16;
}