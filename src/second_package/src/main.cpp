//
// Created by ozkan on 11.01.2021.
//

#include "second_package.h"


int main(int argc, char * argv[])
{

    rclcpp::init(argc, argv);
    CanReceiver receiver;
    receiver.setUp();

    std::chrono::milliseconds timespan(1);

    while (true) {
        //std::cout << "Hello!!" << std::endl;

        receiver.receive();
        std::this_thread::sleep_for(timespan);
    }


    //rclcpp::spin(std::make_shared<OzkanNode>());
    rclcpp::shutdown();
    return 0;
}

