#ifndef APPLANIX_DRIVER_CANRECEIVER_H
#define APPLANIX_DRIVER_CANRECEIVER_H

#include <memory>

#include "rclcpp/rclcpp.hpp"

#include "std_msgs/msg/float32.hpp"
#include "std_msgs/msg/u_int8.hpp"

#include "std_msgs/msg/string.hpp"

#include <socketcan/socket_can_receiver.hpp>
#include <socketcan/socket_can_sender.hpp>
#include <socketcan/socket_can_id.hpp>

using autoware::drivers::socketcan::SocketCanReceiver;
using autoware::drivers::socketcan::SocketCanSender;

using autoware::drivers::socketcan::CanId;
using autoware::drivers::socketcan::StandardFrame;
using autoware::drivers::socketcan::ExtendedFrame;
using autoware::drivers::socketcan::FrameType;

class CanReceiver  : public rclcpp::Node {
public:
    CanReceiver();
    ~CanReceiver();

    void setUp();
    void receive();
    void send(uint32_t ID, uint8_t DLC, uint8_t *DataP);

    struct{
        float Right;
        float Left;
    }WheelSpeed;
    float SteeringAngle;
    float MotorTorque;
    uint8_t VehicleSpeed_kph;

    struct {
        float Voltage;
        float Current;
        float Consumption;
    }Battery;


private:

    std::unique_ptr<SocketCanReceiver> receiver_{};
    std::unique_ptr<SocketCanSender> sender_{};
    std::chrono::milliseconds send_timeout_{1LL};
    std::chrono::milliseconds receive_timeout_{10LL};


    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr wheelspeed_left_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr wheelspeed_right_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr steer_angle_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr bat_volt_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr bat_cur_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr bat_cons_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr motor_torque_;
    rclcpp::Publisher<std_msgs::msg::UInt8>::SharedPtr   veh_speed_;


    uint16_t byte_to_u16(uint8_t * P);
    int16_t  byte_to_i16(uint8_t *P);

    size_t count_;
};


#endif //APPLANIX_DRIVER_CANRECEIVER_H
